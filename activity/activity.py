class EdgeListGraph:
    def __init__(self):
        self.edges = {}

    def add_edge(self, source, target, distance):
        if source not in self.edges:
            self.edges[source] = []
        self.edges[source].append((target, distance))

    def print_edges(self):
        for source, targets in self.edges.items():
            for target, distance in targets:
                print(f"{source} -> {target}: {distance} kilometers")



graph = EdgeListGraph()


graph.add_edge("CA", "MA", 50)
graph.add_edge("FL", "CA", 25)
graph.add_edge("NY", "WA", 70)
graph.add_edge("NY", "NJ", 41)
graph.add_edge("WA", "FL", 67)
graph.add_edge("NJ", "WA", 34)
graph.add_edge("NJ", "NY", 41)


graph.print_edges()

class Graph:
    def __init__(self, nodes):
        self.nodes = nodes
        self.adj_list = {node: [] for node in nodes}

    def add_edge(self, source, destination, weight=None):
        if source in self.adj_list:
            self.adj_list[source].append((destination, weight))
        else:
            raise ValueError("Source node not found in graph.")

    def print_edges(self):
        for node, edges in self.adj_list.items():
            print(f"Edges from node {node}: {', '.join([f'{dest}({weight})' if weight else dest for dest, weight in edges])}")

    def get_degree(self, vertex):
        if vertex in self.adj_list:
            return len(self.adj_list[vertex])
        else:
            raise ValueError("Vertex not found in graph.")



print("Directed Graph")
print()


directed_nodes = ['A', 'B', 'C', 'D', 'E']
directed_graph = Graph(directed_nodes)


directed_graph.add_edge('A', 'B', 2)
directed_graph.add_edge('A', 'C', 3)
directed_graph.add_edge('B', 'D', 1)
directed_graph.add_edge('C', 'E', 4)


print("Graph representation (adjacency list):")
directed_graph.print_edges()


degree = directed_graph.get_degree('A')
print(f"\nDegree of vertex A: {degree}")



print("\nUndirected Graph")
print()


undirected_nodes = ['A', 'B', 'C', 'D', 'E']
undirected_graph = Graph(undirected_nodes)


undirected_graph.add_edge('A', 'B', 2)
undirected_graph.add_edge('A', 'C', 3)
undirected_graph.add_edge('B', 'D', 1)
undirected_graph.add_edge('C', 'E', 4)


print("Graph representation (adjacency list):")
undirected_graph.print_edges()


degree = undirected_graph.get_degree('A')
print(f"\nDegree of vertex A: {degree}")
