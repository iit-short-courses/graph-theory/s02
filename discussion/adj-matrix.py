import pandas as pd

class Graph:
	def __init__(self, num_of_nodes, n, directed=True):
		self.m_num_of_nodes = num_of_nodes
		self.m_directed = directed

		self.m_adj_matrix = [[0 for column in range(num_of_nodes)] for row in range(num_of_nodes)]

		self.adj_df = pd.DataFrame(self.m_adj_matrix, columns=[c for c in n], index=[r for r in n])

	def add_edge(self, node1, node2, weight=1):
		self.adj_df[node2][node1] = weight

		if not self.m_directed:
			self.adj_df[node2][node2] = weight

	def print_adj_matrix(self):
		print(self.adj_df)

n = [0,1,2,3,4]
n = ['A','B','C','D','E']

graph = Graph(5, n, directed=True)
'''
graph.add_edge(0, 0, 25)
graph.add_edge(0, 1, 5)
graph.add_edge(0, 2, 3)
graph.add_edge(1, 3, 1)
graph.add_edge(1, 4, 15)
graph.add_edge(4, 2, 7)
graph.add_edge(4, 3, 11)
'''
graph.add_edge('A', 'A', 25)
graph.add_edge('A', 'B', 5)
graph.add_edge('A', 'C', 3)
graph.add_edge('B', 'D', 1)
graph.add_edge('B', 'E', 15)
graph.add_edge('E', 'C', 7)
graph.add_edge('E', 'D', 11)



graph.print_adj_matrix()